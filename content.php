<?php

// cek apakah module sudah ada apa belum diparameter
$module = empty($_GET['module']) ? 'home' : $_GET['module'];

switch($module) {
    case '/':
    include 'dasboard.php';
    break;
// module dokter
    case 'dokter':
    include 'modules/dokter/index.php';
    break;
    case 'dokter-create':
    include 'modules/dokter/create.php';
    break;
    case 'dokter-show':
    include 'modules/dokter/show.php';
    break;
    case 'dokter-edit':
    include 'modules/dokter/edit.php';
    break;
    case 'dokter-delete':
    include 'modules/dokter/delete.php';
    break;
// module pasien
    case 'pasien':
    include 'modules/pasien/index.php';
    break;
    case 'pasien-create':
    include 'modules/pasien/create.php';
    break;
    case 'pasien-show':
    include 'modules/pasien/show.php';
    break;
    case 'pasien-edit':
    include 'modules/pasien/edit.php';
    break;
    case 'pasien-delete':
    include 'modules/pasien/delete.php';
    break;
// module pegawai
    case 'pegawai':
    include 'modules/pegawai/index.php';
    break;
    case 'pegawai-create':
    include 'modules/pegawai/create.php';
    break;
    case 'pegawai-show':
    include 'modules/pegawai/show.php';
    break;
    case 'pegawai-edit':
    include 'modules/pegawai/edit.php';
    break;
    case 'pegawai-delete':
    include 'modules/pegawai/delete.php';
    break;
// module pendaftaran
    case 'pendaftaran':
    include 'modules/pendaftaran/index.php';
    break;
    case 'pendaftaran-create':
    include 'modules/pendaftaran/create.php';
    break;
    case 'pendaftaran-show':
    include 'modules/pendaftaran/show.php';
    break;
    case 'pendaftaran-edit':
    include 'modules/pendaftaran/edit.php';
    break;
    case 'pendaftaran-delete':
    include 'modules/pendaftaran/delete.php';
    break;
// module pemeriksaan
    case 'pemeriksaan':
    include 'modules/pemeriksaan/index.php';
    break;
    case 'pemeriksaan-create':
    include 'modules/pemeriksaan/create.php';
    break;
    case 'pemeriksaan-show':
    include 'modules/pendaftaran/show.php';
    break;
    case 'pemeriksaan-edit':
    include 'modules/pemeriksaan/edit.php';
    break;
    case 'pemeriksaan-delete':
    include 'modules/pemeriksaan/delete.php';
    break;

// module poliklinik
    case 'poliklinik':
    include 'modules/poli/index.php';
    break;
    case 'poliklinik-create':
    include 'modules/poli/create.php';
    break;
    case 'poliklinik-show':
    include 'modules/poli/show.php';
    break;
    case 'poliklinik-edit':
    include 'modules/poli/edit.php';
    break;
    case 'poliklinik-delete':
    include 'modules/poli/delete.php';
    break;

// module jenis biaya
    case 'jenisbiaya':
    include 'modules/jenisbiaya/index.php';
    break;
    case 'jenisbiaya-create':
    include 'modules/jenisbiaya/create.php';
    break;
    case 'jenisbiaya-show':
    include 'modules/jenisbiaya/show.php';
    break;
    case 'jenisbiaya-edit':
    include 'modules/jenisbiaya/edit.php';
    break;
    case 'jenisbiaya-delete':
    include 'modules/jenisbiaya/delete.php';
    break;

// module jadwal praktek
    case 'jadwalpraktek':
    include 'modules/jadwalpraktek/index.php';
    break;
    case 'jadwalpraktek-create':
    include 'modules/jadwalpraktek/create.php';
    break;
    case 'jadwalpraktek-show':
    include 'modules/jadwalpraktek/show.php';
    break;
    case 'jadwalpraktek-edit':
    include 'modules/jadwalpraktek/edit.php';
    break;
    case 'jadwalpraktek-delete':
    include 'modules/jadwalpraktek/delete.php';
    break;

// module resep
    case 'resep':
    include 'modules/resep/index.php';
    break;
    case 'resep-create':
    include 'modules/resep/create.php';
    break;
    case 'resep-show':
    include 'modules/resep/show.php';
    break;
    case 'resep-edit':
    include 'modules/resep/edit.php';
    break;
    case 'resep-delete':
    include 'modules/resep/delete.php';
    break;

// module obat
    case 'obat':
    include 'modules/obat/index.php';
    break;
    case 'obat-create':
    include 'modules/obat/create.php';
    break;
    case 'obat-show':
    include 'modules/obat/show.php';
    break;
    case 'obat-edit':
    include 'modules/obat/edit.php';
    break;
    case 'obat-delete':
    include 'modules/obat/delete.php';
    break;

    // Jika module tidak ditemukan maka di redirect ke home 
    default: include'dasboard.php';  
}
?>