<div class="sidebar" data-active-color="rose" data-background-color="black" data-image="assets/img/sidebar-1.jpg">
            <div class="sidebar" data-active-color="rose" data-background-color="black" data-image="assets/img/sidebar-1.jpg">
                <div class="logo">
                    <a href="http://www.github.com/bagustriwidiyanto" class="simple-text logo-mini">

                    </a>
                    <a href="http://www.github.com/bagustriwidiyanto" class="simple-text logo-normal">
                        I Clinic
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <div class="user">
                        <div class="photo">
                            <img src="assets/img/faces/avatar.jpg" />
                        </div>
                        <div class="info">
                            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                                <span>
                                    Tania Andrew
                                    <b class="caret"></b>
                                </span>
                            </a>
                            <div class="clearfix"></div>
                            <div class="collapse" id="collapseExample">
                                <ul class="nav">
                                    <li>
                                        <a href="#">
                                            <span class="sidebar-mini"> MP </span>
                                            <span class="sidebar-normal"> My Profile </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="sidebar-mini"> EP </span>
                                            <span class="sidebar-normal"> Edit Profile </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="sidebar-mini"> S </span>
                                            <span class="sidebar-normal"> Settings </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <ul class="nav">
                        <li class="active">
                            <a href="home.php">
                                <i class="material-icons">dashboard</i>
                                <p> Dashboard </p>
                            </a>
                        </li>
                        <li>
                            <a href="index.php?module=dokter">
                                <i class="material-icons">local_hospital</i>
                                <p>Dokter </p>
                            </a>
                        </li>
                        <li>
                            <a href="index.php?module=pasien">
                                <i class="material-icons">person_pin</i>
                                <p> Pasien </p>
                            </a>
                        </li>
                        <li>
                            <a href="index.php?module=pegawai">
                                <i class="material-icons">people</i>
                                <p> Pegawai</p>
                            </a>
                        </li>
                        <li>
                            <a href="index.php?module=pendaftaran">
                                <i class="material-icons">content_paste</i>
                                <p> Pendaftaran </p>
                            </a>
                        </li>
                        <li>
                            <a href="index.php?module=pemeriksaan">
                                <i class="material-icons">work</i>
                                <p> Pemeriksaan </p>
                            </a>
                        </li>
                        <li>
                            <a href="index.php?module=poliklinik">
                                <i class="material-icons">store</i>
                                <p> Poli</p>
                            </a>
                        </li>
                        <li>
                            <a href="index.php?module=jenisbiaya">
                                <i class="material-icons">attach_money</i>
                                <p> Jenis Biaya</p>
                            </a>
                        </li>
                        <li>
                            <a href="index.php?module=jadwalpraktek">
                                <i class="material-icons">schedule</i>
                                <p> Jadwal Praktek Dokter </p>
                            </a>
                        </li>                               
                        <li>
                            <a href="index.php?module=resep">
                                <i class="material-icons">description</i>
                                <p> Resep </p>
                            </a>
                        </li>
                        <li>
                            <a href="index.php?module=obat">
                                <i class="material-icons">opacity</i>
                                <p> Obat</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
       