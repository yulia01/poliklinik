<?php
ob_start();
?>
<div class="content">
   <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
               <div class="card">
                   <div class="card-header card-header-icon" data-background-color="rose">
                       <i class="material-icons">assignment</i>
                 </div>
                 <div class="card-content">
                       <h4 class="card-title">Simple Table</h4>
                     <div class="table-responsive">
<?php require_once("database.php"); ?>
<form action="" method="post">
  <!-- field kode -->
  <div class="table-responsive">
    <div class="small-6 cell">
     <table class="table">
         <thead class="text-primary">
            <th>Id</th>
             <th>Kode</th>
             <th>Hari</th>
             <th>Jam Mulai</th>
             <th>Jam Selesai</th>
        </thead>
         <thead>
             <tr>
                <th>
                     <?php
                      $db = new Database();
                      $db->selectMax('jadwal_praktek','id');
                      $res = $db->getResult();
                      $kode = $res[0]['max'] < 1 ? $res[0]['max']+1  : $res[0]['max']+1;
                      $value = '1'.$kode;
                      echo "<input type='text' class='form-control' name='id' value='$value' placeholder='id' readonly>";
                    ?>
                </th>
                <th><input type="text" class="form-control" value name="kode" placeholder="Kode" required></th>
                <th><input type="text" class="form-control" value name="hari" placeholder="Hari" required></th>
                <th><input type="text" class="form-control" value name="jam_mulai" placeholder="Jam Mulai" required></th>
                <th><input type="text" class="form-control" value name="jam_selesai" placeholder="Jam Selesai" required></th>
             </tr>
        </thead>
    </table>
</div>
</div>
  <!-- Aksi -->
  <div class="grid-x grid-padding-x">
    <div class="small-3 cell">
      <label for="nama" class="text-right middle"></label>
    </div>
    <div class="small-6 cell">
    <div class="card-content">
  <button class="btn" type="submit" name="submit">Simpan</button>
  <button class="btn" type="reset">Reset</button>
  <a class="btn" href='javascript:self.history.back();'>Kembali</a>
</div>
    </div>
  </div>
</form>
<?php 

// check action submit
if(isset($_POST['submit'])){
  $id = $_POST['id'];
  $kode = $_POST['kode'];
  $hari = $_POST['hari'];
  $mulai = $_POST['jam_mulai'];
  $selesai = $_POST['jam_selesai'];
  
  $db=new Database();
  $db->insert('jadwal_praktek',array('id'=>$id, 'kode'=>$kode, 'hari'=>$hari, 'jam_mulai'=>$mulai, 'jam_selesai'=>$selesai));
  $res=$db->getResult();
  // redirect to list
  ?>
    <meta http-equiv="refresh" content="0; url=index.php?module=jadwalpraktek">
<?php 
}
?>