<div class="content">
   <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
               <div class="card">
                   <div class="card-header card-header-icon" data-background-color="rose">
                       <i class="material-icons">assignment</i>
                 </div>
                 <div class="card-content">
                       <h4 class="card-title">Simple Table</h4>
                     <div class="table-responsive">
<?php require_once("database.php");

ob_start();

$id=$_GET['id'];
$db = new Database();
$db->select('jadwal_praktek','*','','', "id=$id");
$res= $db->getResult();
if(count($res) == 0){
  echo "<b>Tidak ada data yang tersedia</b>";
}else{
  foreach ($res as &$r){?> 

<form action="" method="post">
 <!-- field kode -->
 <table class="table">
         <thead class="text-primary">
             <th>Kode</th>
             <th>Hari</th>
             <th>Jam Mulai</th>
             <th>Jam Selesai</th>
         </thead>
         <thead>
             <tr>
                <th>
                  <input type="hidden"  name="id" value="<?php echo $r['id']; ?>">
                  <input type="text" class="form-control" name="kode" placeholder="kode" value="<?php echo $r['kode']; ?>" required>
                </th>
                <th><input type="text" class="form-control" name="hari" placeholder="Hari" value="<?php echo $r['hari']; ?>" required></th>
                <th><input type="text" class="form-control" name="jam_mulai" placeholder="Jam Mulai" value="<?php echo $r['jam_selesai']; ?>" required></th>
                <th><input type="text" class="form-control" name="jam_selesai" placeholder="Jam Selesai" value= "<?php echo $r['jam_mulai'] ;?>" required></th>
             </tr>
        </thead>
    </table>
  <!-- Aksi -->
  <div class="grid-x grid-padding-x">
    <div class="small-3 cell">
      <label for="nama" class="text-right middle"></label>
    </div>
    <div class="small-6 cell">
		<div class="small button-group">
  <button class="btn" type="submit" name="submit">Simpan</button>
  <a class="btn" href='javascript:self.history.back();'>Kembali</a>
</div>
    </div>
  </div>
</form>
<?php
              }
          }
          ?>
<?php 
// check action submit
if(isset($_POST['submit'])){
  $id = $_POST['id'];
  $kode = $_POST['kode'];
  $hari = $_POST['hari'];
  $mulai = $_POST['jam_mulai'];
  $selesai = $_POST['jam_selesai'];
  $db = new Database();
  $db->update('jadwal_praktek',array(
    'kode'=>$kode,
    'hari'=>$hari,
    'jam_mulai'=>$mulai,
    'jam_selesai'=>$selesai,
  ),
    "id=$id"
  );
  $res = $db->getResult();
  // print_r($res);
  ?>
    <meta http-equiv="refresh" content="0; url=index.php?module=jadwalpraktek">
<?php 
}

?>