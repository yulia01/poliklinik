<div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                    <a href="index.php?module=jadwalpraktek-create" class="btn" role="button"> Create</a>
                    <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Table Jadwal Praktek Dokter</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="text-primary">
                                        <tr>
                                        <th>Kode</th>
                                        <th>Hari</th>
                                        <th>Jam Mulai</th>
                                        <th>Jam Selesai</th>
                                        <th class="text-right">Actions</th>
                                        </tr>
                                    </thead>
                                        <?php
                                          require_once("database.php");
                                          $db=new Database();
                                          $db->select('jadwal_praktek', '*');
                                          $res=$db->getResult();
                                            if(count($res) == 0){ ?>
                                                <tr>
                                                    <td colspan="8">Tidak ada data yang tersedia </td>
                                                </tr>
                                            <?php
                                                }else{
                                                foreach ($res as &$r){?>
                                                <tr>
                                                    <td><?php echo $r['kode'] ?></td>
                                                    <td><?php echo $r['hari'] ?></td>
                                                    <td><?php echo $r['jam_mulai'] ?></td>
                                                    <td><?php echo $r['jam_selesai'] ?></td>
                                                    <td class="td-actions text-right">
                                                        <button type="btn" rel="tooltip" class="btn btn-info">
                                                            <a href="?module=jadwalpraktek-show&id=<?php echo $r['id']; ?>" class=" button">
                                                                <i class="material-icons">description</i>
                                                            </a>
                                                        </button>
                                                        <button type="btn" rel="tooltip" class="btn btn-success">
                                                            <a href="?module=jadwalpraktek-edit&id=<?php echo $r['id']; ?>" class="secondary button">
                                                                <i class="material-icons">edit</i>
                                                            </a>
                                                        </button>
                                                        <button type="btn" rel="tooltip" class="btn btn-danger">
                                                            <a href="?module=jadwalpraktek-delete&id=<?php echo $r['id']; ?>"onClick='return confirm("Apakah yakin menghapus?")' class="alert button">
                                                                <i class="material-icons">delete</i>
                                                            </a>
                                                        </button>
                                                    </td>
                                                </tr>
                                        <?php
                                                      }
                                                  }
                                                  ?>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
