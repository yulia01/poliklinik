<div class="content">
   <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
               <div class="card">
                   <div class="card-header card-header-icon" data-background-color="rose">
                       <i class="material-icons">assignment</i>
                 </div>
                 <div class="card-content">
                       <h4 class="card-title">Simple Table</h4>
                     <div class="table-responsive">
<?php require_once("database.php");

ob_start();

$id=$_GET['id'];
$db = new Database();
$db->select('obat','*','','', "id=$id");
$res= $db->getResult();
if(count($res) == 0){
  echo "<b>Tidak ada data yang tersedia</b>";
}else{
  foreach ($res as &$r){?> 

<form action="" method="post">
 <!-- field kode -->
 <table class="table">
         <thead class="text-primary">
             <th>Kode</th>
             <th>Nama</th>
             <th>Merk</th>
             <th>Satuan</th>
             <th>Harga Jual</th>
         </thead>
         <thead>
             <tr>
                <th>
                  <input type="hidden"  name="id" value="<?php echo $r['id']; ?>">
                  <input type="text" class="form-control" name="kode" placeholder="kode" value="<?php echo $r['kode']; ?>" required>
                </th>
                <th><input type="text" class="form-control" name="nama" placeholder="Nama" value="<?php echo $r['nama']; ?>" required></th>
                <th><input type="text" class="form-control" name="merk" placeholder="merk" value="<?php echo $r['merk']; ?>" required></th>
                <th><input type="text" class="form-control" name="satuan" value= "<?php echo $r['satuan'] ;?>" required></th>
                <th><input type="text" class="form-control" name="harga_jual" value= "<?php echo $r['harga_jual'] ;?>" required></th>
             </tr>
        </thead>
    </table>
  <!-- Aksi -->
  <div class="grid-x grid-padding-x">
    <div class="small-3 cell">
      <label for="nama" class="text-right middle"></label>
    </div>
    <div class="small-6 cell">
		<div class="small button-group">
  <button class="btn" type="submit" name="submit">Simpan</button>
  <a class="btn" href='javascript:self.history.back();'>Kembali</a>
</div>
    </div>
  </div>
</form>
<?php
              }
          }
          ?>
<?php 
// check action submit
if(isset($_POST['submit'])){
  $id = $_POST['id'];
  $kode = $_POST['kode'];
  $nama = $_POST['nama'];
  $merk = $_POST['merk'];
  $satuan = $_POST['satuan'];
  $harga = $_POST['harga_jual'];

  $db = new Database();
  $db->update('obat',array(
    'kode'=>$kode,
    'nama'=>$nama,
    'merk'=>$merk,
    'satuan'=>$satuan,
    'harga_jual'=>$harga,
  ),
    "id=$id"
  );
  $res = $db->getResult();
  // print_r($res);
  ?>
    <meta http-equiv="refresh" content="0; url=index.php?module=obat">
<?php 
}

?>