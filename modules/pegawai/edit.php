<div class="content">
   <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
               <div class="card">
                   <div class="card-header card-header-icon" data-background-color="rose">
                       <i class="material-icons">assignment</i>
                 </div>
                 <div class="card-content">
                       <h4 class="card-title">Simple Table</h4>
                     <div class="table-responsive">
<?php require_once("database.php");

ob_start();

$id=$_GET['id'];
$db = new Database();
$db->select('pegawai','*','','', "id=$id");
$res= $db->getResult();
if(count($res) == 0){
  echo "<b>Tidak ada data yang tersedia</b>";
}else{
  foreach ($res as &$r){?> 

<form action="" method="post">
 <!-- field kode -->
 <table class="table">
         <thead class="text-primary">
             <th>No</th>
             <th>Nama</th>
             <th>Alamat</th>
             <th>Telpon</th>
             <th>Tanggal Lahir</th>
             <th>Gender</th>
         </thead>
         <thead>
             <tr>
                <th>
                  <input type="hidden"  name="id" value="<?php echo $r['id']; ?>">
                  <input type="text" class="form-control" name="no" placeholder="no" value="<?php echo $r['no']; ?>" required>
                </th>
                <th><input type="text" class="form-control" name="nama" placeholder="Nama" value="<?php echo $r['nama']; ?>" required></th>
                <th><input type="text" class="form-control" name="alamat" placeholder="alamat" value="<?php echo $r['alamat']; ?>" required></th>
                <th><input type="text" class="form-control" name="telpon" value= "<?php echo $r['telpon'] ;?>" required></th>
                <th><input type="date" class="form-control datetimepicker" value="10/05/2016"  name="tgl_lahir" placeholder="Tanggal Lahir" value="<?php echo $r['tgl_lahir']; ?>" required></th>
                <th><input type="text" class="form-control" name="gender" placeholder="Gender" value="<?php echo $r['gender']; ?>" required></th>
             </tr>
        </thead>
    </table>
  <!-- Aksi -->
  <div class="grid-x grid-padding-x">
    <div class="small-3 cell">
      <label for="nama" class="text-right middle"></label>
    </div>
    <div class="small-6 cell">
		<div class="small button-group">
  <button class="btn" type="submit" name="submit">Simpan</button>
  <a class="btn" href='javascript:self.history.back();'>Kembali</a>
</div>
    </div>
  </div>
</form>
<?php
              }
          }
          ?>
<?php 
// check action submit
if(isset($_POST['submit'])){
  $id = $_POST['id'];
  $no = $_POST['no'];
  $nama = $_POST['nama'];
  $alamat = $_POST['alamat'];
  $telpon = $_POST['telpon'];
  $tgl = $_POST['tgl_lahir'];
  $gender = $_POST['gender'];
  $db = new Database();
  $db->update('pegawai',array(
    'no'=>$no,
    'nama'=>$nama,
    'alamat'=>$alamat,
    'telpon'=>$telpon,
    'tgl_lahir'=>$tgl,
    'gender'=>$gender,
  ),
    "id=$id"
  );
  $res = $db->getResult();
  // print_r($res);
  ?>
    <meta http-equiv="refresh" content="0; url=index.php?module=pegawai">
<?php 
}

?>