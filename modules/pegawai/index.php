<div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                    <a href="index.php?module=pegawai-create" class="btn" role="button"> Create</a>
                    <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Table Pegawai</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="text-primary">
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Alamat</th>
                                            <th>Telpon</th>
                                            <th>Tanggal Lahir</th>
                                            <th>Gender</th>
                                            <th class="text-right">Actions</th>
                                        </tr>
                                    </thead>
                                        <?php
                                          require_once("database.php");
                                          $db=new Database();
                                          $db->select('pegawai', 'id, no, nama, alamat, telpon, tgl_lahir, gender');
                                          $res=$db->getResult();
                                            if(count($res) == 0){ ?>
                                                <tr>
                                                    <td colspan="8">Tidak ada data yang tersedia </td>
                                                </tr>
                                            <?php
                                                }else{
                                                foreach ($res as &$r){?>
                                                <tr>
                                                    <td><?php echo $r['no'] ?></td>
                                                    <td><?php echo $r['nama'] ?></td>
                                                    <td><?php echo $r['alamat'] ?></td>
                                                    <td><?php echo $r['telpon'] ?></td>
                                                    <td><?php echo $r['tgl_lahir'] ?></td>
                                                    <td><?php echo $r['gender'] ?></td>
                                                    <td class="td-actions text-right">
                                                        <button type="btn" rel="tooltip" class="btn btn-info">
                                                            <a href="?module=pegawai-show&id=<?php echo $r['id']; ?>" class=" button">
                                                                <i class="material-icons">description</i>
                                                            </a>
                                                        </button>
                                                        <button type="btn" rel="tooltip" class="btn btn-success">
                                                            <a href="?module=pegawai-edit&id=<?php echo $r['id']; ?>" class="secondary button">
                                                                <i class="material-icons">edit</i>
                                                            </a>
                                                        </button>
                                                        <button type="btn" rel="tooltip" class="btn btn-danger">
                                                            <a href="?module=pegawai-delete&id=<?php echo $r['id']; ?>"onClick='return confirm("Apakah yakin menghapus?")' class="alert button">
                                                                <i class="material-icons">delete</i>
                                                            </a>
                                                        </button>
                                                    </td>
                                                </tr>
                                        <?php
                                                      }
                                                  }
                                                  ?>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
