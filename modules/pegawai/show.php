<div class="content">
<div class="container-fluid">
<?php require_once("database.php");

ob_start();
?> 

<div class="grid-x grid-padding-x">
<?php
$id=$_GET['id'];
$db = new Database();
$db->select('pegawai','*','','', "id=$id");
$res= $db->getResult();
// print_r($res);
if(count($res) == 0){ ?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-icon" data-background-color="rose">
                <i class="material-icons">assignment</i>
            </div>
            <div class="card-content">
                <h4 class="card-title">Table Pegawai</h4>
                <div class="table-responsive">
                    <table class="table">
                      <tbody>
                        <tr>
                          <td>Data yang anda cari tidak ada atau terhapus</td>
                        </tr>
                      </tbody>
                    </table>
</div>
<?php }else{
  foreach ($res as &$r){ 
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-icon" data-background-color="rose">
                <i class="material-icons">assignment</i>
            </div>
            <div class="card-content">
                <h4 class="card-title">Table Pasien</h4>
                <div class="table-responsive">
                  <table class="table">
                       <thead class="text-primary">
                           <td>No</td>
                           <td>Nama</td>
                           <td>Alamat</td>
                           <td>Telepon</td>
                           <td>Tanggal Lahir</td>
                           <td>Gender</td>
                      </thead>
                      <tbody>
                           <tr>
                              <td><?php echo $r['no']; ?></td>
                              <td><?php echo $r['nama']; ?></td>
                              <td><?php echo $r['alamat']; ?></td>
                              <td><?php echo $r['telpon']; ?></td>  
                              <td><?php echo $r['tgl_lahir']; ?></td>
                              <td><?php echo $r['gender']; ?></td>
                            </tr>
                      </tbody>
                  </table>
<a class="btn" href="javascript:printDiv('print-area');" >Print</a>
<a href="index.php?module=pegawai-delete&id=<?php echo $r['id']; ?>"onClick='return confirm("Apakah yakin menghapus?")' class="alert btn">Delete</a>
<a class="btn" href='javascript:self.history.back();'>Kembali</a>
</div>
<?php }}?>

<style>
@media print {
   * { color: black; background: white; }
   table { font-size: 80%; }
}
</style>

<iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>

<script type="text/javascript">
     
     function printDiv(elementId) {
    var a = document.getElementById('print-area').value;
    var b = document.getElementById(elementId).innerHTML;
    window.frames["print_frame"].document.title = document.title;
    window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
    window.frames["print_frame"].window.focus();
    window.frames["print_frame"].window.print();
}
</script>
</div>
</div>