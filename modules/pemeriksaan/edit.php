<div class="content">
   <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
               <div class="card">
                   <div class="card-header card-header-icon" data-background-color="rose">
                       <i class="material-icons">assignment</i>
                 </div>
                 <div class="card-content">
                       <h4 class="card-title">Simple Table</h4>
                     <div class="table-responsive">
<?php require_once("database.php");

ob_start();

$id=$_GET['id'];
$db = new Database();
$db->select('pemeriksaan','*','','', "id=$id");
$res= $db->getResult();
if(count($res) == 0){
  echo "<b>Tidak ada data yang tersedia</b>";
}else{
  foreach ($res as &$r){?> 

<form action="" method="post">
 <!-- field kode -->
 <table class="table">
         <thead class="text-primary">
             <th>Keluhan</th>
             <th>Diagnosa</th>
             <th>Perawatan</th>
             <th>Tindakan</th>
             <th>Berat Badan</th>
             <th>Tensi Diastolik</th>
             <th>Tensi Sistolik</th>
         </thead>
         <thead>
             <tr>
                <th><input type="text" class="form-control" name="keluhan" placeholder="keluhan" value="<?php echo $r['keluhan']; ?>" required></th>
                <th><input type="text" class="form-control" name="diagnosa" placeholder="diagnosa" value="<?php echo $r['diagnosa']; ?>" required></th>
                <th><input type="text" class="form-control" name="perawatan" placeholder="perawatan" value= "<?php echo $r['perawatan'] ;?>" required></th>
                <th><input type="text" class="form-control" name="tindakan" placeholder="tindakan" value="<?php echo $r['tindakan']; ?>" required></th>
                <th><input type="text" class="form-control" name="berat_badan" placeholder="berat badan" value="<?php echo $r['berat_badan']; ?>" required></th>
                <th><input type="text" class="form-control" name="tensi_diastolik" placeholder="tensi diastolik" value="<?php echo $r['tensi_diastolik']; ?>" required></th>
                <th><input type="text" class="form-control" name="tensi_sistolik" placeholder="tensi sistolik" value="<?php echo $r['tensi_sistolik']; ?>" required></th>
             </tr>
        </thead>
    </table>
  <!-- Aksi -->
  <div class="grid-x grid-padding-x">
    <div class="small-3 cell">
      <label for="nama" class="text-right middle"></label>
    </div>
    <div class="small-6 cell">
		<div class="small button-group">
  <button class="btn" type="submit" name="submit">Simpan</button>
  <a class="btn" href='javascript:self.history.back();'>Kembali</a>
</div>
    </div>
  </div>
</form>
<?php
              }
          }
          ?>
<?php 
// check action submit
if(isset($_POST['submit'])){
    $kel = $_POST['keluhan'];
    $diag = $_POST['diagnosa'];
    $per = $_POST['perawatan'];
    $tin = $_POST['tindakan'];
    $ber = $_POST['berat_badan'];
    $dias = $_POST['tensi_diastolik'];
    $sis = $_POST['tensi_sistolik'];
  $db = new Database();
  $db->update('pemeriksaan',array(
    'keluhan'=>$kel,
    'diagnosa'=>$diag,
    'perawatan'=>$per,
    'tindakan'=>$tin,
    'berat_badan'=>$ber,
    'tensi_diastolik'=>$dias,
    'tensi_sistolik'=>$sis,
  ),
    "id=$id"
  );
  $res = $db->getResult();
  // print_r($res);
   ?>
   <meta http-equiv="refresh" content="0; url=index.php?module=pemeriksaan">
 <?php 
}

?>