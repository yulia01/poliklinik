<div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                    <a href="index.php?module=pemeriksaan-create" class="btn" role="button"> Create</a>
                    <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Table Pemeriksaan</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="text-primary">
                                        <tr>
                                        <th>Keluhan</th>
                                        <th>Diagnosa</th>
                                        <th>Perawatan</th>
                                        <th>Tindakan</th>
                                        <th>Berat Badan</th>
                                        <th>Tensi Diastolik</th>
                                        <th>Tensi Sistolik</th>
                                        <th class="text-right">Actions</th>
                                        </tr>
                                    </thead>
                                        <?php
                                          require_once("database.php");
                                          $db=new Database();
                                          $db->select('pemeriksaan', 'id, keluhan, diagnosa, perawatan, tindakan, berat_badan, tensi_diastolik, tensi_sistolik');
                                          $res=$db->getResult();
                                            if(count($res) == 0){ ?>
                                                <tr>
                                                    <td colspan="8">Tidak ada data yang tersedia </td>
                                                </tr>
                                            <?php
                                                }else{
                                                foreach ($res as &$r){?>
                                                <tr>
                                                    <td><?php echo $r['keluhan'] ?></td>
                                                    <td><?php echo $r['diagnosa'] ?></td>
                                                    <td><?php echo $r['perawatan'] ?></td>
                                                    <td><?php echo $r['tindakan'] ?></td>
                                                    <td><?php echo $r['berat_badan'] ?></td>
                                                    <td><?php echo $r['tensi_diastolik'] ?></td>
                                                    <td><?php echo $r['tensi_sistolik'] ?></td>
                                                    <td class="td-actions text-right">
                                                        <button type="btn" rel="tooltip" class="btn btn-info">
                                                            <a href="?module=pemeriksaan-show&id=<?php echo $r['id']; ?>" class=" button">
                                                                <i class="material-icons">description</i>
                                                            </a>
                                                        </button>
                                                        <button type="btn" rel="tooltip" class="btn btn-success">
                                                            <a href="?module=pemeriksaan-edit&id=<?php echo $r['id']; ?>" class="secondary button">
                                                                <i class="material-icons">edit</i>
                                                            </a>
                                                        </button>
                                                        <button type="btn" rel="tooltip" class="btn btn-danger">
                                                            <a href="?module=pemeriksaan-delete&id=<?php echo $r['id']; ?>"onClick='return confirm("Apakah yakin menghapus?")' class="alert button">
                                                                <i class="material-icons">delete</i>
                                                            </a>
                                                        </button>
                                                    </td>
                                                </tr>
                                        <?php
                                                      }
                                                  }
                                                  ?>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
