<div class="content">
   <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
               <div class="card">
                   <div class="card-header card-header-icon" data-background-color="rose">
                       <i class="material-icons">assignment</i>
                 </div>
                 <div class="card-content">
                       <h4 class="card-title">Simple Table</h4>
                     <div class="table-responsive">
<?php require_once("database.php");

ob_start();

$id=$_GET['id'];
$db = new Database();
$db->select('pendaftaran','*','','', "id=$id");
$res= $db->getResult();
if(count($res) == 0){
  echo "<b>Tidak ada data yang tersedia</b>";
}else{
  foreach ($res as &$r){?> 

<form action="" method="post">
 <!-- field kode -->
 <table class="table">
         <thead class="text-primary">
            <th>Id</th>
             <th>Tanggal Registrasi</th>
             <th>No</th>
             <th>id_antrian</th>
             <th>id_pasien</th>
             <th>id_dokter</th>
             <th>id_pegawai</th>
         </thead>
         <thead>
             <tr>
                <th><input type="hidden"  name="id" value="<?php echo $r['id']; ?>"></th>
                <th><input type="date" class="form-control datetimepicker" value="10/05/2016"  name="tgl_registrasi" placeholder="Tanggal Registrasi" value="<?php echo $r['tgl_registrasi']; ?>" required></th>
                <th><input type="text" class="form-control" name="no" placeholder="no" value="<?php echo $r['no']; ?>" required></th>
                <th><input type="text" class="form-control" name="id_antrian" placeholder="id_antrian" value="<?php echo $r['id_antrian']; ?>" required></th>
                <th><input type="text" class="form-control" name="id_pasien" placeholder="id_pasien" value="<?php echo $r['id_pasien']; ?>" required></th>
                <th><input type="text" class="form-control" name="id_dokter" placeholder="id_dokter" value= "<?php echo $r['id_dokter'] ;?>" required></th>
                <th><input type="text" class="form-control" name="id_pegawai" placeholder="id_pegawai" value="<?php echo $r['id_pegawai']; ?>" required></th>
             </tr>
        </thead>
    </table>
  <!-- Aksi -->
  <div class="grid-x grid-padding-x">
    <div class="small-3 cell">
      <label for="nama" class="text-right middle"></label>
    </div>
    <div class="small-6 cell">
		<div class="small button-group">
  <button class="btn" type="submit" name="submit">Simpan</button>
  <a class="btn" href='javascript:self.history.back();'>Kembali</a>
</div>
    </div>
  </div>
</form>
<?php
              }
          }
          ?>
<?php 
// check action submit
if(isset($_POST['submit'])){
    $id = $_POST['id'];
    $tgl = $_POST['tgl_registrasi'];
    $no = $_POST['no'];
    $antrian = $_POST['id_antrian'];
    $pasien = $_POST['id_pasien'];
    $dokter = $_POST['id_dokter'];
    $pegawai = $_POST['id_pegawai'];
  $db = new Database();
  $db->update('pendaftaran',array(
    'tgl_registrasi'=>$tgl,
    'no'=>$no,
    'id_antrian'=>$antrian,
    'id_pasien'=>$pasien,
    'id_dokter'=>$dokter,
    'id_pegawai'=>$pegawai,
  ),
    "id=$id"
  );
  $res = $db->getResult();
  // print_r($res);
  ?>
    <meta http-equiv="refresh" content="0; url=index.php?module=pendaftaran">
<?php 
}

?>