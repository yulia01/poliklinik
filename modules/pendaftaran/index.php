<div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                    <a href="index.php?module=pendaftaran-create" class="btn" role="button"> Create</a>
                    <div class="card-header card-header-icon" data-background-color="rose">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title">Table pendaftaran</h4>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="text-primary">
                                        <tr>
                                            <th>Tanggal Registrasi</th>
                                            <th>No</th>
                                            <th>id_antrian</th>
                                            <th>id_pasien</th>
                                            <th>id_dokter</th>
                                            <th>id_pegawai</th>
                                            <th class="text-right">Actions</th>
                                        </tr>
                                    </thead>
                                        <?php
                                          require_once("database.php");
                                          $db=new Database();
                                          $db->select('pendaftaran', 'id, tgl_registrasi, no, id_antrian, id_pasien, id_dokter, id_pegawai');
                                          $res=$db->getResult();
                                            if(count($res) == 0){ ?>
                                                <tr>
                                                    <td colspan="8">Tidak ada data yang tersedia </td>
                                                </tr>
                                            <?php
                                                }else{
                                                foreach ($res as &$r){?>
                                                <tr>
                                                    <td><?php echo $r['tgl_registrasi'] ?></td>
                                                    <td><?php echo $r['no'] ?></td>
                                                    <td><?php echo $r['id_antrian'] ?></td>
                                                    <td><?php echo $r['id_pasien'] ?></td>
                                                    <td><?php echo $r['id_dokter'] ?></td>
                                                    <td><?php echo $r['id_pegawai'] ?></td>
                                                    <td class="td-actions text-right">
                                                        <button type="btn" rel="tooltip" class="btn btn-info">
                                                            <a href="?module=pendaftaran-show&id=<?php echo $r['id']; ?>" class=" button">
                                                                <i class="material-icons">description</i>
                                                            </a>
                                                        </button>
                                                        <button type="btn" rel="tooltip" class="btn btn-success">
                                                            <a href="?module=pendaftaran-edit&id=<?php echo $r['id']; ?>" class="secondary button">
                                                                <i class="material-icons">edit</i>
                                                            </a>
                                                        </button>
                                                        <button type="btn" rel="tooltip" class="btn btn-danger">
                                                            <a href="?module=pendaftaran-delete&id=<?php echo $r['id']; ?>"onClick='return confirm("Apakah yakin menghapus?")' class="alert button">
                                                                <i class="material-icons">delete</i>
                                                            </a>
                                                        </button>
                                                    </td>
                                                </tr>
                                        <?php
                                                      }
                                                  }
                                                  ?>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
