<?php
ob_start();
?>
<div class="content">
   <div class="container-fluid">
       <div class="row">
           <div class="col-md-12">
               <div class="card">
                   <div class="card-header card-header-icon" data-background-color="rose">
                       <i class="material-icons">assignment</i>
                 </div>
                 <div class="card-content">
                       <h4 class="card-title">Simple Table</h4>
                     <div class="table-responsive">
<?php require_once("database.php"); ?>
<form action="" method="post">
  <!-- field kode -->
  <div class="table-responsive">
    <div class="small-6 cell">
     <table class="table">
         <thead class="text-primary">
            <th>Id</th>
             <th>Dosis</th>
             <th>Jumlah</th>
         </thead>
         <thead>
             <tr>
                <th>
                     <?php
                      $db = new Database();
                      $db->selectMax('resep','id');
                      $res = $db->getResult();
                      $kode = $res[0]['max'] < 1 ? $res[0]['max']+1  : $res[0]['max']+1;
                      $value = '1'.$kode;
                      echo "<input type='text' class='form-control' name='id' value='$value' placeholder='id' readonly>";
                    ?>
                </th>
                <th><input type="text" class="form-control" value name="dosis" placeholder="Dosis" required></th>
                <th><input type="text" class="form-control" value name="jumlah" placeholder="Jumlah" required></th>
             </tr>
        </thead>
    </table>
</div>
</div>
  <!-- Aksi -->
  <div class="grid-x grid-padding-x">
    <div class="small-3 cell">
      <label for="nama" class="text-right middle"></label>
    </div>
    <div class="small-6 cell">
    <div class="card-content">
  <button class="btn" type="submit" name="submit">Simpan</button>
  <button class="btn" type="reset">Reset</button>
  <a class="btn" href='javascript:self.history.back();'>Kembali</a>
</div>
    </div>
  </div>
</form>
<?php 

// check action submit
if(isset($_POST['submit'])){
  $id = $_POST['id'];
  $dosis = $_POST['dosis'];
  $jumlah = $_POST['jumlah'];
  
  $db=new Database();
  $db->insert('resep',array('id'=>$id, 'dosis'=>$dosis, 'jumlah'=>$jumlah));
  $res=$db->getResult();
  // redirect to list
  ?>
    <meta http-equiv="refresh" content="0; url=index.php?module=resep">
<?php 
}
?>